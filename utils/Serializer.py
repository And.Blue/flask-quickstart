from typing import Dict, Any, Tuple, Union
from config import get_serializer_config_files, get_serializer_config

SERIALIZABLE_TYPES = {
    str, int, float, bool, None
}


class Serializer:
    """
        Verifies that object attributes have a primitive type. And attempts to deserialize attributes that are non primitive
    """

    def serialize(self):
        object_attributes: Dict = {}

        for attribute in self.__dict__:
            object_attribute_value = self.__dict__[attribute]  # value of the objects attribute
            # verifies if the attribute matches a type that is serializable
            if type(object_attribute_value) in SERIALIZABLE_TYPES:
                object_attributes[attribute] = self.__dict__[attribute]
            # checks if the attribute intherits from de Serialize class in order to execute attribute_name.deserialize
            elif object_attribute_value.__class__.__bases__[0] == Serializer.__mro__[0]:
                object_attributes[attribute] = object_attribute_value.serialize()
            else:
                object_attributes[attribute] = None
        return object_attributes

    """
        From data injected as dictionary binds key (and sub keys if provided) to the object representation
    """

    def deserialize(self, data: Dict, with_profile: bool = True):
        # if with_profile:
        #     self.__deserialize_with_profile(data)
        # iterates through the dictonary potentially containing keys that correspond to the object's attribute names
        # else:
        # iterates through the dictonary potentially containing keys that correspond to the object's attribute names
        for key in data:
            if key in self.__dict__:
                # verifies if dictionary's value works
                if type(data[key]) in SERIALIZABLE_TYPES:
                    self.__dict__[key] = data[key]
                elif Serializer.__mro__[0] == self.__dict__[key].__class__.__bases__[0]:
                    self.__dict__[key].deserialize(data[key])

    # def __deserialize_with_profile(self, data: Dict):
    #     has_profile, profile = self.__get_profile()
    #
    #     for key in data:
    #         if key in profile.values():
    #             obj_attribute = [att for (att, value) in profile.items() if value == key]
    #             if obj_attribute:
    #                 if type(self.__dict__[obj_attribute[0]]) in SERIALIZABLE_TYPES:
    #                     self.__dict__[obj_attribute[0]] = data[key]

    """
        Enables us to find and bind attributes to serializer profiles. 
    """

    def __get_profile(self) -> Tuple[bool, Union[Dict[str, Any], None]]:
        config = None
        profile_configs: Dict[str, Any] = get_serializer_config()  # global config for each object
        has_profile: bool = all([profile_configs[self.__class__.__name__.lower()]])  # if object has a certain profile
        if has_profile:
            return True, profile_configs[self.__class__.__name__.lower()]
        return False, None
