from flask import make_response, jsonify

from typing import Dict, Union

JSON_MIME_TYPE = 'application/json'


def json_response(data=None, message: str = "", status=200, headers=None):
    headers = headers or {}
    if 'Content-Type' not in headers:
        headers['Content-Type'] = JSON_MIME_TYPE
    if not data:
        data = {}

    if message:
        data["message"] = message

    return make_response(jsonify(data), status, headers)


def json_error(message: str) -> str:
    return json_response(data={'error': message})
