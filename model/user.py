from .address import Address

"""
Defines what a user is
"""


class User:
    def __init__(self, first_name: str, company_name: str, last_name: str, address_ob: Address, phone1: str, phone: str,
                 email: str):
        self.first_name = first_name
        self.last_name = last_name
        self.company_name = company_name
        self.address = address_ob
        self.phone = phone
        self.phone1 = phone1
        self.email = email


