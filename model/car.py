from utils import Serializer


class Car(Serializer):
    def __init__(self):
        self.name: str = ''
        self.color: str = ''
        self.seat: Seat = Seat()


class Seat(Serializer):
    def __init__(self):
        self.is_blue: str = ''
        self.head_rest: HeadRest = HeadRest()


class HeadRest(Serializer):
    def __init__(self):
        self.color: str = ''
