"""
ADDRESS CONTAINED IN USER
"""


class Address:
    def __init__(self, address: str, city: str, county: str, state: str, zip: int):
        self.address = address
        self.city = city
        self.county = county
        self.state = state
        self.zip = zip

    def __str__(self):
        return f'{self.address},{self.county},{self.county},{self.state},{self.zip}'
