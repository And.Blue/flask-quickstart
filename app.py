from flask import Flask
from utils import json_response
from routes import words, users, view_config

app = Flask(__name__)
app.url_map.strict_slashes = False


@app.route('/', methods=['GET', 'POST'])
def index():
    return "App Works"


# Other modules (blueprints)
app.register_blueprint(words, url_prefix='/words')
app.register_blueprint(users, url_prefix='/users')
app.register_blueprint(view_config, url_prefix='/config')


# errors
@app.errorhandler(404)
def not_found(e):
    return json_response(status=404)


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
