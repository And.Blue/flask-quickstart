# Flask Quickstart
This Project is a flask app to test out the framework and how to use it with other python modules.



### Run with Flask (For dev)
The following commands should get you up and running, with hot reload (not having to restart app at each change in the code).
Make sure that you are running with Python 3.6 >= 
`pip install` to install dependencies (`pip install flask-debug` might be required as well)
`export FLASK_APP=app.py` to tell flask 
`export FLASK_ENV=development` to enable the development profile and 
`flask run` to run the APP



### Run with docker (for demos and prod)
Build the app's container 
`docker build -t flask-quickstart:latest .`

run the container on port 5000! 
`docker run -d -p 5000:5000 flask-quickstart`
You can now query the API by hitting _http://localhost:5000/_