from flask import Blueprint, request
from services import get_csv_line

from model import User, Address, Car
from config import get_file_path
from utils import json_response

users = Blueprint('users', __name__)


@users.route('/', methods=['GET'])
def index():
    car = Car()
    data = {"color": "Merco",
            "name": "Hamilton",
            "seat": {
                "is_blue": False,
                "head_rest": {
                        "color": "green"}
            }}

    car.deserialize(data)

    return json_response(car.serialize())


@users.route('/<int:user_id>')
def get_user(user_id):
    csv_line = get_csv_line(user_id, get_file_path())

    # first_name: str, company_name: str, last_name: str, address_ob: Address, phone1: str, phone: str
    # self, address: str, city: str, county: str, state: str, zip: int
    user = User(csv_line['first_name'], csv_line['company_name'], csv_line['last_name'],
                Address(csv_line['address'], csv_line['city'], csv_line['county'], csv_line['state'], csv_line['zip']),
                csv_line['phone1'], csv_line['phone'], csv_line['email'])

    return json_response(user)


@users.route('/', methods=['POST'])
def create_user():
    body = request.data

    return
