from flask import Blueprint, request
from services import read_from_key, write_to_key, delete_word_from_key
from typing import Dict

from utils import json_response, json_error

words = Blueprint('words', __name__)


@words.route('/')
def get_words():
    word_list = read_from_key("random_words")
    return json_response({"random_words": word_list})


@words.route('/', methods=['POST'])
def add_word():
    data: Dict = request.json
    # POST boy does not contain word key
    if not all([data.get('word')]):
        return json_error('request body must contain \'word\' key')

    word_to_add = data.get('word')

    if write_to_key(word_to_add, "random_words"):
        return json_response(message=f'successfully added {word_to_add} to list of random words in config.json')

    return json_error('Something went wrong')


@words.route('/', methods=['DELETE'])
def remove_word():
    data: Dict = request.json
    # POST boy does not contain word key
    if not all([data.get('word')]):
        return json_error('request body must contain \'word\' key')

    word_to_delete = data.get('word')

    if delete_word_from_key("random_words", word_to_delete):
        return json_response(message=f'successfully deleted {word_to_delete} to list of random words in config.json')
    return json_error(f'Something went wrong, could not delete "{word_to_delete}", it must not exist in list.')
