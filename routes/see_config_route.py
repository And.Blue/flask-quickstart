from flask import Blueprint

from config import get_serializer_config_files, get_serializer_config
from utils import json_response

view_config = Blueprint('config', __name__)


@view_config.route('/', methods=['GET'])
def index():
    return json_response(message="Config index")


@view_config.route('/serializer/profiles', methods=['GET'])
def serializer_profiles():
    profile_config = get_serializer_config()
    return json_response(data=profile_config)


@view_config.route('/serializer/profile-files', methods=['GET'])
def serializer_profile_files():
    file_names = get_serializer_config_files()
    return json_response(data={"files": file_names}, message="Files containing the list of profiles.")
