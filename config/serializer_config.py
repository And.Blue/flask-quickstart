from os import listdir
from os.path import isfile, join

from typing import List, Dict, Any
from services import read_from_key, load_from_file

"""
    Assembles within a single object for each entity declared in the serializer profiles each entity attribute and
     the targeted name. The serializer therefore can bind a file's value to an object attribute directly.
"""


def get_serializer_config() -> Dict[str, Any]:
    files: List[str] = get_serializer_config_files()
    profiles = {}

    for file in files:
        profile_file = load_from_file(file)
        for profile_entry in profile_file:
            profiles[profile_entry['profile']] = {i['class_attribute']: i['targeted_name'] for i in
                                                  profile_entry['class_to_file']}
    return profiles


def get_serializer_config_files():
    path = read_from_key('serializer_profiles_dir')
    return [f'{path}/{f}' for f in listdir(path) if isfile(join(path, f))]
