import json
from typing import List, Union, NoReturn
from services import read_from_key

"""
Reads and gets the config file paths
"""

def get_file_path(multiple_paths: bool = False) -> Union[str, List[str]]:
    if multiple_paths:
        return read_from_key('file_paths')
    return read_from_key('file_paths')[0]
