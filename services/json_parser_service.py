import json

from typing import List, Dict, Union, NoReturn

"""
Basic READ and WRITES to config.json (or another file if specified)
"""


def load_from_file(file: str) -> Dict:
    with open(file) as file:
        return json.load(file)


def write_to_file(file: str, content: Union[List, Dict]) -> NoReturn:
    with open(file, 'w+') as file:
        json.dump(content, file)


def read_from_key(json_object_key: str, file: str = 'config.json') -> Union[List[str], str]:
    file = load_from_file(file)
    return file[json_object_key]


def delete_word_from_key(json_object_key: str, word_to_delete: str, file: str = 'config.json') -> bool:
    file_content = load_from_file(file)
    # checks if not in list
    if not all(file_content[json_object_key]):
        return False
    # checks if word exits and deletes it
    if word_to_delete in file_content[json_object_key]:
        file_content[json_object_key].remove(word_to_delete)
        write_to_file(file, file_content)
        return True
    return False


def write_to_key(word: str, json_object_key: str, file: str = 'config.json') -> bool:
    file_content = load_from_file(file)

    if json_object_key not in file_content:
        return False

    if type(file_content[json_object_key]) is list:
        file_content[json_object_key].append(word)
        write_to_file(file, file_content)
        return True
    return False
