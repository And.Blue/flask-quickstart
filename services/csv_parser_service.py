import csv


def get_csv(selected_csv_file: str):
    with open(selected_csv_file) as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            print(row)

    return ''


def get_csv_line(line_nb: int, selected_csv_file: str):
    with open(selected_csv_file) as csv_file:
        line_count = 0
        csv_reader = csv.DictReader(csv_file, delimiter=',')
        for row in csv_reader:
            if line_count == line_nb:
                print(row)
                return row
            line_count += 1
